# Lancement
* Clonez le repo dans un dossier sia
```
git clone git@gitlab.com:sia-insa-lyon/dev/keycloak-theme.git sia
```
* Déplacez ce dossier dans `<racine de Keycloak>/themes/`
* Sélectionnez le thème sia dans les paramètres du Realm

### Tester vos modifs
dans `standalone/configuration/standalone.xml`, modifier le tag `<theme>` comme suit
```xml
<theme>
    <staticMaxAge>-1</staticMaxAge>
    <cacheThemes>false</cacheThemes>
    <cacheTemplates>false</cacheTemplates>
	<dir>${jboss.home.dir}/themes</dir>
	<welcomeTheme>sia</welcomeTheme>
</theme>
```
Ça ralentit Keycloak, mais ça permet de voir instantanément les modifications
(par défaut Keycloak a un cache)
## Modifications
IntelliJ IDEA est compatible par défaut avec les fichiers FreeMarker (`.ftl`)

Voir [ici](https://www.keycloak.org/docs/9.0/server_development/#theme-types) pour la documentation Keycloak.